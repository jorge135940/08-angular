import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contador',
  templateUrl: './contador.component.html',
  styleUrls: ['./contador.component.css']
})
export class ContadorComponent implements OnInit {
  num: number = 0;
  mensaje: string = ''

  suma():void {
    this.mensaje='' 
    this.num === 50? this.mensaje ='El valor no puede ser mayor a 50':
      this.num++;
  }
  
  resta():void {
    this.mensaje='' 
    this.num === 0? this.mensaje ='El valor no puede ser menor a 50':
      --this.num ;
  }

  constructor() {}

  ngOnInit(): void {
  }
}

